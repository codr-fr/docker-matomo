include .env

default: help

.PHONY: help
help:  ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

.PHONY: project-init
project-init: ## Init local files.
	@echo "$(COLOR_LIGHT_GREEN)Init local files...$(COLOR_NC)"
	rsync -avz --ignore-existing ./example.env ./.env

.PHONY: docker-pull
docker-pull: ## Update container images.
	@echo "$(COLOR_LIGHT_GREEN)Update containers images for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) pull

.PHONY: docker-up
docker-up: ## Start containers.
	@echo "$(COLOR_LIGHT_GREEN)Starting up containers for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) up

.PHONY: docker-upd
docker-upd: ## Start containers in detached mode.
	@echo "$(COLOR_LIGHT_GREEN)Starting up containers for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) up -d

.PHONY: docker-stop
docker-stop: ## Stop containers.
	@echo "$(COLOR_LIGHT_GREEN)Stopping containers for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) stop

.PHONY: docker-down
docker-down: ## Remove containers.
	@echo "$(COLOR_LIGHT_GREEN)Removing containers for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) down

.PHONY: docker-prune
docker-prune: ## Remove containers, volumes and images.
	@echo "$(COLOR_LIGHT_GREEN)Removing containers, volumes and images for $(COMPOSE_PROJECT_NAME)...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) down -v --rmi all

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
